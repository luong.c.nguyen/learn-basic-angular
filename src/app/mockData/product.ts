export const productList = [
  {
    id: 1,
    name: 'iPhone 14 Pro Max',
    image: '../../assets/ip4.jpg',
    description:
      'iPhone 14 Pro Max 512GB 5G 6.7 inch 3 camera 12MP 4K Video 6GB RAM 512GB ROM',
    price: 1000,
  },
  {
    id: 2,
    name: 'iPhone 15 Pro Max',
    image: '../../assets/ip15.jpg',
    description:
      'iPhone 15 Pro Max 512GB 5G 6.7 inch 3 camera 12MP 4K Video 6GB RAM 512GB ROM',
    price: 2000,
  },
  {
    id: 3,
    name: 'Samsung Galaxy S20',
    image: '../../assets/samsung.jpg',
    description:
      'Samsung Galaxy S20 5G 6.2 inch 3 camera 12MP 4K Video 6GB RAM 128GB ROM',
    price: 3000,
  },
  {
    id: 4,
    name: 'Vivo V20',
    image: '../../assets/vivo.jpg',
    description:
      'Vivo V20 5G 6.2 inch 3 camera 12MP 4K Video 6GB RAM 128GB ROM',
    price: 500,
  },
  {
    id: 5,
    name: 'Oppo F20',
    image: '../../assets/oppo.jpg',
    price: 200,
  },
];
