import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from '../../../types/types';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-product-alert',
  templateUrl: './product-alert.component.html',
  styleUrl: './product-alert.component.css',
})
export class ProductAlertComponent {
  // Get data from parent component
  @Input() product: Product | undefined;

  // Send data to parent component
  @Output() notify = new EventEmitter();
}
