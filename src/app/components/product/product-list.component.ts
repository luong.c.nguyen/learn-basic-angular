import { Component } from '@angular/core';
import { Product } from '../../../types/types';
import { productList } from '../../mockData/product';

@Component({
  selector: 'app-product',
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.css',
})
export class ProductListComponent {
  products: Product[] = productList;

  share() {
    window.alert('The product has been shared!');
  }

  onNotify() {
    window.alert('You will be notified when the product goes on sale');
  }
}
