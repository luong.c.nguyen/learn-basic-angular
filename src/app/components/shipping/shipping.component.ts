import { Observable } from 'rxjs';
import { CartService } from './../../services/cart.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrl: './shipping.component.css',
})
export class ShippingComponent implements OnInit {
  // ! (k cần khai báo giá trị ban đầu)
  shippingCosts!: Observable<{ type: string; price: number }[]>;

  constructor(private CartService: CartService) {}

  ngOnInit() {
    this.shippingCosts = this.CartService.getShippingPrices();
  }

  getShippingPrice(price: number) {
    return this.CartService.getShippingPrice(price);
  }
}
